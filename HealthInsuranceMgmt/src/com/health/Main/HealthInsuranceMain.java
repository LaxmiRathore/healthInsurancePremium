package com.health.Main;

import java.util.ArrayList;
import java.util.List;

import com.health.Utility.Client;
import com.health.Utility.Gender;
import com.health.Utility.Habbit;
import com.health.Utility.HealthCondition;
import com.health.healthPremium.HealthPremiumCal;

public class HealthInsuranceMain {

	public static void main(String[] args) {
	HealthCondition healthCondition = new HealthCondition("No","No", "No", "Yes");
	
	List<Habbit> habbits = new ArrayList<Habbit>();
	habbits.add(new Habbit("smoking", "Yes"));
	habbits.add(new Habbit("alcoh0l","Yes"));
	habbits.add(new Habbit("dailyExcercise","Yes"));
	habbits.add(new Habbit("drugs","No"));
	
	
	Client client = new 	Client("Norman Gomes", 34, Gender.Male, healthCondition,habbits);
	System.out.println("Health Insurance Premium for Mr. "+client.getName()+" :Rs."+HealthPremiumCal.getHealthPremimum(client));
	

	}

}
