package com.health.Utility;

import java.util.HashMap;
import java.util.List;

public class Client {
private String Name;
private int Age;
private Gender gender;
private HealthCondition healthCondition;
private List<Habbit> habbits;
public Client(String name, int age, Gender gender, HealthCondition healthCondition, List<Habbit> habbits) {
	super();
	Name = name;
	Age = age;
	this.gender = gender;
	this.healthCondition = healthCondition;
	this.habbits = habbits;
}
public String getName() {
	return Name;
}
public void setName(String name) {
	Name = name;
}
public int getAge() {
	return Age;
}
public void setAge(int age) {
	Age = age;
}
public Gender getGender() {
	return gender;
}
public void setGender(Gender gender) {
	this.gender = gender;
}
public HealthCondition getHealthCondition() {
	return healthCondition;
}
public void setHealthCondition(HealthCondition healthCondition) {
	this.healthCondition = healthCondition;
}
public List<Habbit> getHabbits() {
	return habbits;
}
public void setHabbits(List<Habbit> habbits) {
	this.habbits = habbits;
}
@Override
public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + Age;
	result = prime * result + ((Name == null) ? 0 : Name.hashCode());
	result = prime * result + ((gender == null) ? 0 : gender.hashCode());
	result = prime * result + ((habbits == null) ? 0 : habbits.hashCode());
	result = prime * result + ((healthCondition == null) ? 0 : healthCondition.hashCode());
	return result;
}
@Override
public boolean equals(Object obj) {
	if (this == obj)
		return true;
	if (obj == null)
		return false;
	if (getClass() != obj.getClass())
		return false;
	Client other = (Client) obj;
	if (Age != other.Age)
		return false;
	if (Name == null) {
		if (other.Name != null)
			return false;
	} else if (!Name.equals(other.Name))
		return false;
	if (gender != other.gender)
		return false;
	if (habbits == null) {
		if (other.habbits != null)
			return false;
	} else if (!habbits.equals(other.habbits))
		return false;
	if (healthCondition == null) {
		if (other.healthCondition != null)
			return false;
	} else if (!healthCondition.equals(other.healthCondition))
		return false;
	return true;
}
@Override
public String toString() {
	return "Client [Name=" + Name + ", Age=" + Age + ", gender=" + gender + ", healthCondition=" + healthCondition
			+ ", habbits=" + habbits + "]";
}


}
