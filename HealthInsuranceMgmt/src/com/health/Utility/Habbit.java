package com.health.Utility;

public class Habbit {
private String name;
private String haveHabbit;

public Habbit(String name, String haveHabbit) {
	super();
	this.name = name;
	this.haveHabbit = haveHabbit;
}
public String getName() {
	return name;
}
public void setName(String name) {
	this.name = name;
}
public String gethaveHabbit() {
	return haveHabbit;
}
public void sethaveHabbit(String haveHabbit) {
	this.haveHabbit = haveHabbit;
}
@Override
public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((haveHabbit == null) ? 0 : haveHabbit.hashCode());
	result = prime * result + ((name == null) ? 0 : name.hashCode());
	return result;
}
@Override
public boolean equals(Object obj) {
	if (this == obj)
		return true;
	if (obj == null)
		return false;
	if (getClass() != obj.getClass())
		return false;
	Habbit other = (Habbit) obj;
	if (haveHabbit == null) {
		if (other.haveHabbit != null)
			return false;
	} else if (!haveHabbit.equals(other.haveHabbit))
		return false;
	if (name == null) {
		if (other.name != null)
			return false;
	} else if (!name.equals(other.name))
		return false;
	return true;
}
@Override
public String toString() {
	return "Habbit [name=" + name + ", haveHabbit=" + haveHabbit + "]";
}
}
