package com.health.Utility;

public class HealthCondition {
	private String hypertension;
	private String bloodPressure;
	private String bloodSugar;
	private String overWeight;
	public HealthCondition(String hypertension, String bloodPressure, String bloodSugar, String overWeight) {
		super();
		this.hypertension = hypertension;
		this.bloodPressure = bloodPressure;
		this.bloodSugar = bloodSugar;
		this.overWeight = overWeight;
	}
	@Override
	public String toString() {
		return "HealthCondition [hypertension=" + hypertension + ", bloodPressure=" + bloodPressure + ", bloodSugar="
				+ bloodSugar + ", overWeight=" + overWeight + "]";
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((bloodPressure == null) ? 0 : bloodPressure.hashCode());
		result = prime * result + ((bloodSugar == null) ? 0 : bloodSugar.hashCode());
		result = prime * result + ((hypertension == null) ? 0 : hypertension.hashCode());
		result = prime * result + ((overWeight == null) ? 0 : overWeight.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		HealthCondition other = (HealthCondition) obj;
		if (bloodPressure == null) {
			if (other.bloodPressure != null)
				return false;
		} else if (!bloodPressure.equals(other.bloodPressure))
			return false;
		if (bloodSugar == null) {
			if (other.bloodSugar != null)
				return false;
		} else if (!bloodSugar.equals(other.bloodSugar))
			return false;
		if (hypertension == null) {
			if (other.hypertension != null)
				return false;
		} else if (!hypertension.equals(other.hypertension))
			return false;
		if (overWeight == null) {
			if (other.overWeight != null)
				return false;
		} else if (!overWeight.equals(other.overWeight))
			return false;
		return true;
	}
	public String getHypertension() {
		return hypertension;
	}
	public void setHypertension(String hypertension) {
		this.hypertension = hypertension;
	}
	public String getBloodPressure() {
		return bloodPressure;
	}
	public void setBloodPressure(String bloodPressure) {
		this.bloodPressure = bloodPressure;
	}
	public String getBloodSugar() {
		return bloodSugar;
	}
	public void setBloodSugar(String bloodSugar) {
		this.bloodSugar = bloodSugar;
	}
	public String getOverWeight() {
		return overWeight;
	}
	public void setOverWeight(String overWeight) {
		this.overWeight = overWeight;
	}


}
