package com.health.healthPremium;

import java.util.ArrayList;
import java.util.List;

import com.health.Utility.Client;
import com.health.Utility.Habbit;
import com.health.test.*

public class HealthPremiumCal {
	private static int basePremium = 5000;

	public static double getHealthPremimum(Client client) {
	    double totalPremium = 0;
	    try{
		 totalPremium = getPremiumByAge(client) + getPremiumByGender(client) + getPremiumByHealthCondition(client)
				+ getPremiumByHabbits(client);
	    }catch(Exception e){
	        e.printStackTrace();
	    }
		return totalPremium;
	}

	private static double getPremiumByHealthCondition(Client client) throws  NullPointerException{
		double premium = basePremium;
		double hypertension = 0;
		double bp = 0;
		double bs = 0;
		double ow = 0;
        try{
		if (client.getHealthCondition().getHypertension() == "Yes")
			hypertension = premium * 0.1;
		if (client.getHealthCondition().getBloodPressure() == "Yes")
			bp = premium * 0.01;
		if (client.getHealthCondition().getBloodSugar() == "Yes")
			bs = premium * 0.01;
		if (client.getHealthCondition().getOverWeight() == "Yes")
			ow = premium * 0.01;
        }catch(NullPointerException e){
            throw new NullPonterException("Client info empty")
            System.out.println(e)
        }
		return hypertension + bp + bs + ow;
	}

	private static double getPremiumByAge(Client client) throws InvalidAgeException{
		double premium = basePremium;
		if(client.getAge() <= 0)  
         throw new InvalidAgeException("not valid"); 
		if (client.getAge() < 18)
			return premium;
		if (client.getAge() >= 18 && client.getAge() < 25) {
			premium = premium + premium * 0.1;
		}
		if (client.getAge() >= 25 && client.getAge() < 30) {
			premium = premium + premium * 0.1 + (premium + premium * 0.1) * 0.1;
		}
		if (client.getAge() >= 30 && client.getAge() < 35)
			premium = premium + (premium * 0.1) + ((premium + premium * 0.1) * 0.1)
					+ (premium + (premium * 0.1) + ((premium + premium * 0.1) * 0.1)) * 0.1;

		return premium;
	}

	public static double getPremiumByGender(Client client) {
		if (client.getGender().name().equals("Male"))
			return basePremium * 0.02;
		return 0;
	}

	private static double getPremiumByHabbits(Client client) {
		List<Habbit> l = client.getHabbits();
		List<String> ghabbit = new ArrayList<String>();
		double dE = 0;
		double smoking = 0;
		double drug = 0;
		double drink = 0;
		ghabbit.add("dailyExcercise");
		for (Habbit h : l) {
			if (ghabbit.contains(h.getName()) && h.gethaveHabbit().equals("Yes")) {
				dE = basePremium * 0.03;

			}
			if (!ghabbit.contains(h.getName()) && h.gethaveHabbit().equals("Yes")) {
				if (h.getName().equals("smoking"))
					smoking = basePremium * 0.03;
				if (h.getName().equals("drug"))
					drug = basePremium * 0.03;
				if (h.getName().equals("alcohol"))
					drink = basePremium * 0.03;

			}

		}

		return smoking + drug + drink - dE;
	}

}
