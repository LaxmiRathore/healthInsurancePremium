package com.health.test;
import java.nio.channels.AsynchronousServerSocketChannel;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import com.health.Utility.Client;
import com.health.Utility.Gender;
import com.health.Utility.Habbit;
import com.health.Utility.HealthCondition;
import com.health.healthPremium.HealthPremiumCal;

import static org.junit.Assert.assertEquals;



public class HealthInsuranceTest{
	List<Habbit> habbits = new ArrayList<Habbit>();
	HealthCondition healthCondition = new HealthCondition("No","No", "No", "Yes");
	
	Client client = new Client("Norman Gomes", 34, Gender.Male, healthCondition,habbits);
	
	@Test
	public void testGetHealthPremimum() {
		habbits.add(new Habbit("smoking", "Yes"));
		habbits.add(new Habbit("alcoh0l","Yes"));
		habbits.add(new Habbit("dailyExcercise","Yes"));
		habbits.add(new Habbit("drugs","No"));		
		
		double expected = 6805;
		double sum = HealthPremiumCal.getHealthPremimum(client);		
		assertEquals(sum, expected,0.0);

		}
	@Test
	   public void testPremiumAppliedByGender() {
		double calResult= HealthPremiumCal.getPremiumByGender(client);
		if(!client.getGender().equals("Male")) {
			assertEquals(100, calResult, 0.0);
		}
	}
	@Test
	   public void testPremiumForAgeLessThen18() {
		Client c = new Client("laxmi", 18, Gender.Male, healthCondition,habbits);
		double calResult1 = HealthPremiumCal.getHealthPremimum(c);
		assertEquals(5000, calResult1,0.0);
	
	     
	   }
	 // to test method shoudh throw NullpointerException when client object is null
	@Test(expected = NullPointerException.class)
	    public void testMethodShouldThrowNullPointerException(){
	        Client c =null;
	        HealthPremiumCal.getPremiumByHealthCondition(c);
	    }
	//to test method shoudh throw InvalidAgeException when if age supplied is less then 1 year
	@Test(expected = InvalidAgeException.class)
	    public void testMethodShouldThrowInvalidAgeException(){
	        Client c = new Client("laxmi", -1, Gender.Male, healthCondition,habbits);
	        HealthPremiumCal.getPremiumByAge(c);
	    }

}
